var Flag = function(x, y, type, state) {
    var spr = "";
    if (type === "destination") spr = R.image.flag_red;

    var f = game.add.sprite(x + 35, y + 35, spr);

    f.animations.add(R.anim.idle, [0, 1], 2, true);
    f.animations.play(R.anim.idle);

    game.physics.p2.enable(f);
    f.body.static = true;

    f.body.setCollisionGroup(state.coll_objects);
    f.body.collides(state.coll_objects);

    f.body.onBeginContact.add(function(other, _1, _2, _3, _4) {
        if (other.sprite.isBox) {
            state.score++;
            other.sprite.destroy();

            AddMedals(f.x - 35, f.y - 35, 5);
        }
    });

    return f;
}