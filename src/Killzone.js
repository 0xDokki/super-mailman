var Killzone = function(obj, state) {
    var kz = game.add.sprite(obj.x + obj.width / 2, obj.y + obj.height / 2, R.image.blank);
    kz.alpha = 0;
    kz.width = obj.width;
    kz.height = obj.height;

    game.physics.p2.enable(kz);
    kz.body.static = true;

    kz.body.setCollisionGroup(state.coll_objects);
    kz.body.collides([state.coll_objects, state.coll_player]);

    kz.type = obj.properties["killtype"];

    kz.body.onBeginContact.add(function(other, _1, _2, _3, _4) {
        if (kz.type === "fire")
            Fire(other.x, other.y, other.sprite);

        Die(other.sprite);

        if (other.sprite.isBox) state.boxesLost++;

        if (other.sprite.isPlayer) {
            game.time.events.add(3000, function() {
                game.state.start(R.state.gameover);
            });
        }
    });
};