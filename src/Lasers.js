var Switch = function(x, y, color, active, state) {
    var sw = game.add.sprite(x, y, R.image.switch);

    var offset = 0;
    if (color === "green") offset = 2;
    else if (color === "red") offset = 4;
    else if (color === "yellow") offset = 6;

    sw.animations.add(R.anim.idle, [offset], 1);
    sw.animations.add(R.anim.active, [offset + 1], 1);
    sw.animations.play(R.anim.idle);

    sw.sfx = game.add.audio(R.audio.laser.main);

    sw.isActive = false;
    sw.toggle = function() {
        sw.isActive = !sw.isActive;
        AddMedals(sw.x, sw.y, 1);
        sw.sfx.play();

        if (sw.isActive) sw.animations.play(R.anim.active);
        else sw.animations.play(R.anim.idle);

        state.LASERS[color].forEach(function(laser) {
            laser.toggle();
        });
    };

    if (active)
        game.time.events.add(500, function() { sw.toggle(); });

    state.SWITCHES.push(sw);
    return sw;
};

var Laser = function(x, y, width, height, orientation, color, state) {
    var lb = game.add.sprite(x + width / 2, y + height / 2, R.image.laser_beam);
    lb.width = width + ((!orientation) * 10);
    lb.height = height + (orientation * 10);
    lb.actual_y = y + height / 2;

    var offset = 0;
    if (color === "green") offset = 3;
    else if (color === "red") offset = 6;
    else if (color === "yellow") offset = 9;

    lb.frame = offset + orientation;

    game.physics.p2.enable(lb);
    lb.body.static = true;

    lb.body.clearShapes();
    lb.body.addRectangle((orientation ? 18 : width), (orientation ? height : 18), 0, 0);

    lb.body.setCollisionGroup(state.coll_objects);
    lb.body.collides([state.coll_objects, state.coll_player]);

    lb.isActive = false;
    lb.body.y = -9000;
    lb.set = function(active) {
        lb.isActive = active;
        lb.body.y = active ? lb.actual_y : -9000;
    };
    lb.toggle = function() {
        lb.isActive = !lb.isActive;
        lb.body.y = lb.isActive ? lb.actual_y : -9000;
    };

    state.LASERS[color].push(lb);
    return lb;
};

var LaserSource = function(x, y, direction, state) {
    var ls = game.add.sprite(x + 35, y + 35, R.image.laser);
    ls.bringToTop();

    var offset = 0;
    if (direction === "down") offset = 2;
    else if (direction === "left") offset = 4;
    else if (direction === "right") offset = 6;

    ls.animations.add(R.anim.idle, [offset, offset + 1], 2, true);
    ls.animations.play(R.anim.idle);

    game.physics.p2.enable(ls);
    ls.body.static = true;

    ls.body.setCollisionGroup(state.coll_objects);
    ls.body.collides([state.coll_objects, state.coll_player]);

    return ls;
};