var Player = function (x, y, num, state) {
    var player = game.add.sprite(x, y, (num === 1 ? R.image.player : R.image.player2));
    player.anchor.setTo(.5, .5);
    player.isPlayer = true;

    player.animations.add(R.anim.idle, [10], 1, false);
    player.animations.add(R.anim.walk, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 15, true);
    player.animations.add(R.anim.jump, [11], 1, false);
    player.animations.add(R.anim.hurt, [12], 1, false);
    player.animations.add(R.anim.duck, [13], 1, false);

    game.camera.follow(player);

    game.physics.p2.enable(player);
    player.body.fixedRotation = true;
    player.body.linearDamping = 1;
    player.body.collideWorldBounds = true;

    player.body.setCollisionGroup(state.coll_player);
    player.body.collides([state.coll_objects, state.coll_ground, state.coll_barr_player, state.coll_player]);

    player.cursor = createInputCursorForPlayer(num);

    player.flipped = false;

    player.jmpSound = game.add.audio(R.audio.jump);

    player.handleInput = function () {
        player.body.velocity.x = 0;

        if (player.cursor.up.isDown && player.body.canJump()) {
            player.body.velocity.y = -450;
            player.jmpSound.play();
        } else if (player.cursor.down.isDown && !player.body.canJump()) {
            player.body.velocity.y = 450;
        }

        if (this.cursor.left.isDown) {
            player.body.velocity.x = -300;
        } else if (this.cursor.right.isDown) {
            player.body.velocity.x = 300;
        }

        if (Math.abs(player.body.velocity.y) > 0.5 && !player.body.canJump()) player.animations.play(R.anim.jump);
        else if (player.body.velocity.x > 0) player.animations.play(R.anim.walk);
        else if (player.body.velocity.x < 0) player.animations.play(R.anim.walk);
        else player.animations.play("idle");

        if (player.body.velocity.x > 0) {
            if (player.flipped === true) player.flip(false);
            player.flipped = false;
        } else if (player.body.velocity.x < 0) {
            if (player.flipped === false) player.flip(true);
            player.flipped = true;
        }
    };

    player.body.canJump = function () {
        var yAxis = p2.vec2.fromValues(0, 1);
        var result = false;
        for (var i = 0; i < game.physics.p2.world.narrowphase.contactEquations.length; i++) {
            var c = game.physics.p2.world.narrowphase.contactEquations[i];

            if (c.bodyA === player.body.data || c.bodyB === player.body.data) {
                var d = p2.vec2.dot(c.normalA, yAxis);
                if (c.bodyA === player.body.data) d *= -1;
                if (d > 0.5) result = true;
            }
        }
        return result;
    };

    player.flip = function (isFlipped) {
        if (isFlipped) {
            game.add.tween(player.scale).to({x: -1}, 250, Phaser.Easing.Cubic.InOut, true);
        } else {
            game.add.tween(player.scale).to({x: 1}, 250, Phaser.Easing.Cubic.InOut, true);
        }
    };

    player.overlapMany = function(others) {
        for (var o in others) {
            if (others.hasOwnProperty(o))
                if (others[o].getBounds().intersects(player.getBounds())) return others[o];
        }
    };

    player.update = function() {
        var sw = player.overlapMany(state.SWITCHES);
        if (sw) {
            if (!player.switchDisplay) {
                player.switchDisplay = game.add.bitmapText(0, 0, R.font.mario, "interact", 20);
                player.switchDisplay.align = "center";
                player.switchDisplay.anchor.setTo(.5, .5);
            }

            player.switchDisplay.x = player.x;
            player.switchDisplay.y = player.y - 75;
            player.switchDisplay.alpha = 1;

            if (player.cursor.action.justUp)
                sw.toggle();
        } else if (player.switchDisplay) {
            player.switchDisplay.alpha = 0;
        }
    };

    return player;
};