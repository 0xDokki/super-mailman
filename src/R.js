var R = {
    state: {
        preload: "Preloader",
        mainmenu: "MainMenu",
        maingame: "MainGame",
        win: "Win",
        gameover: "GameOver"
    },
    map: {
        start: "map_1",
        layer: {
            tiles: "L1",
            boxes: "boxes",
            interactables: "interactables",
            killzones: "killzones"
        }
    },
    anim: {
        walk: "walk",
        idle: "idle",
        jump: "jump",
        hurt: "hurt",
        duck: "duck",

        active: "active"
    },
    image: {
        title: "title",

        player: "player",
        player2: "player2",

        checkbox: {
            ticked: "chkbx_true",
            unticked: "chkbx_false"
        },

        slider: {
            bar: "sldr_bar",
            end: "sldr_end",
            pin: "sldr_pin"
        },

        springboard: "springboard",
        flag_red: "flag_red",
        switch: "switch",
        laser: "laser",
        laser_beam: "laser_beam",
        barrier: "barrier",
        medals: "medals",

        tileset_base: "tileset_base",

        blank: "blank",

        box: "box",
        box_empty: "box_empty",
        box_warning: "box_warning",

        fireball: "fireball"
    },
    font: {
        mario: "mario"
    },
    audio: {
        jump: "sfx_jump",
        lose: "sfx_lose",
        spring: "sfx_spring",
        win: "sfx_win",
        medals: "sfx_medals",
        laser: {
            main: "sfx_laser",
            _1: "sfx_laser_1",
            _2: "sfx_laser_2",
            _3: "sfx_laser_3"
        }
    }
};