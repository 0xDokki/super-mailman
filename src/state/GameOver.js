var GameOver = function() {};
GameOver.prototype = {
    create: function() {
        game.stage.backgroundColor = "#800000";

        var t1 = game.add.bitmapText(640, 100, R.font.mario, "You failed a Level", 70);
        t1.anchor.setTo(.5, .5);

        this.continue = game.add.bitmapText(640, 200, R.font.mario, "Click to retry", 40);
        this.continue.anchor.setTo(.5, .5);

        var t2 = game.add.bitmapText(640, 230, R.font.mario, "or press (Enter)", 15);
        t2.anchor.setTo(.5, .5);

        this.input = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        var rect = new Phaser.Rectangle(200, 320, 880, 300);
        var p = new Phaser.Point();
        Medals.forEach(function(medal) {
            rect.random(p);
            var spr = game.add.sprite(p.x, p.y, R.image.medals, medal);
            spr.rotation = (game.rnd.frac() * (Math.PI / 2)) - (Math.PI / 4);
            var frac = game.rnd.frac();
            spr.scale.x = 1 + frac;
            spr.scale.y = 1 + frac;
        });

        var sfx = game.add.audio(R.audio.lose);
        sfx.play();

    },
    update: function() {
        if (this.input.isDown) game.state.start(R.state.maingame);

        if (game.input.activePointer.leftButton.isDown)
            game.state.start(R.state.maingame);
    }
};
