var MainGame = function() {};
MainGame.prototype = {
    init: function() {
        game.physics.startSystem(Phaser.Physics.P2JS);
        game.physics.p2.gravity.y = 400;
    },
    preload: function() {
        if (lastMapInfo.next_map) {
            game.cache.removeTilemap(R.map.start);
            game.load.tilemap(R.map.start, DATA_URLS[lastMapInfo.next_map], null, Phaser.Tilemap.TILED_JSON);
        }
    },
    create: function() {
        game.stage.backgroundColor = "#d0f4f7";

        this.map = game.add.tilemap(R.map.start);
        this.map.addTilesetImage("Kenney_base", R.image.tileset_base);

        this.score = 0;
        this.requiredScore = this.map.properties["boxesRequiredForWin"];
        this.boxesLost = 0;
        this.maxBoxesLost = this.map.properties["maxBoxesToBeLost"];

        this.next_map = this.map.properties["next_map"];

        this.mainLayer = this.map.createLayer(R.map.layer.tiles);
        this.mainLayer.resizeWorld();

        this.coll_ground = game.physics.p2.createCollisionGroup();
        this.coll_player = game.physics.p2.createCollisionGroup();
        this.coll_objects = game.physics.p2.createCollisionGroup();
        this.coll_barr_box = game.physics.p2.createCollisionGroup();
        this.coll_barr_player = game.physics.p2.createCollisionGroup();
        game.physics.p2.updateBoundsCollisionGroup();

        for (var layer_key in this.map.collision) {
            if (!this.map.collision.hasOwnProperty(layer_key)) continue;
            var layer = this.map.collision[layer_key];
            for (var index in layer) {
                if (!layer.hasOwnProperty(index)) continue;
                var tile = layer[index];
                if (tile.type !== "coll") continue;

                var coll = game.add.sprite(tile.x, tile.y, R.image.blank);
                coll.alpha = 0;
                game.physics.p2.enable(coll);
                coll.body.static = true;
                coll.body.clearShapes();

                if (tile.polygon) coll.body.addPolygon({}, tile.polygon);
                else if (tile.rectangle) coll.body.addPolygon({}, [[0, 0], [0, 70], [70, 70], [70, 0]]);

                coll.body.setCollisionGroup(this.coll_ground);
                coll.body.collides([this.coll_objects, this.coll_player]);
            }
        }

        for (var box_key in this.map.objects[R.map.layer.boxes]) {
            if (!this.map.objects[R.map.layer.boxes].hasOwnProperty(box_key)) continue;
            var box_obj = this.map.objects[R.map.layer.boxes][box_key];

            var type;
            if (box_obj.properties["type"] === "normal") type = R.image.box;
            if (box_obj.properties["type"] === "empty") type = R.image.box_empty;
            if (box_obj.properties["type"] === "warning") type = R.image.box_warning;

            var box = game.add.sprite(box_obj.x + 35, box_obj.y + 35, type);
            game.physics.p2.enable(box);
            box.body.setCollisionGroup(this.coll_objects);
            box.body.collides([this.coll_objects, this.coll_ground, this.coll_player, this.coll_barr_box]);

            box.isBox = true;
        }

        this.killzones = [];
        for (var killzone_key in this.map.objects[R.map.layer.killzones]) {
            if (!this.map.objects[R.map.layer.killzones].hasOwnProperty(killzone_key)) continue;
            var killzone_obj = this.map.objects[R.map.layer.killzones][killzone_key];

            var killzone = Killzone(killzone_obj, this);

            this.killzones.push(killzone);
        }

        this.LASERS = { blue: [], green: [], red: [], yellow: [] };
        this.SWITCHES = [];
        for (var inter_key in this.map.objects[R.map.layer.interactables]) {
            if (!this.map.objects[R.map.layer.interactables].hasOwnProperty(inter_key)) continue;
            var inter_obj = this.map.objects[R.map.layer.interactables][inter_key];

            var props = inter_obj.properties;
            var type = props["type"];
            if (type === "springboard")
                Springboard(inter_obj.x, inter_obj.y, props["force"], this);
            else if (type === "flag")
                Flag(inter_obj.x, inter_obj.y, props["subtype"], this);
            else if (type === "laser_switch")
                Switch(inter_obj.x, inter_obj.y, props["laser_color"], props["active"], this);
            else if (type === "laser_beam")
                Laser(inter_obj.x, inter_obj.y, inter_obj.width, inter_obj.height, props["orientation"], props["laser_color"], this);
            else if (type === "laser_source")
                LaserSource(inter_obj.x, inter_obj.y, props["direction"], this);
            else if (type === "barrier")
                Barrier(inter_obj.x, inter_obj.y, inter_obj.width, inter_obj.height, props["orientation"], props["sub_type"], this);
        }

        this.player = Player(this.map.properties["spawn_x"], this.map.properties["spawn_y"], 1, this);
        if (lastMapInfo.local_mp) {
            this.player2 = Player(this.map.properties["spawn_x"], this.map.properties["spawn_y"], 2, this);
        }

        this.scoreDisplay = game.add.bitmapText(10, 10, R.font.mario, "Boxes delivered: " + this.score  + "/" + this.requiredScore, 30);
        this.scoreDisplay.fixedToCamera = true;

        this.boxDisplay = game.add.bitmapText(10, 50, R.font.mario, "Boxes lost: " + this.boxesLost  + "/" + this.maxBoxesLost, 30);
        this.boxDisplay.fixedToCamera = true;

        this.restartText = game.add.bitmapText(1270, 10, R.font.mario, "Press (Enter) to restart current level.\n", 16);
        this.restartText.fixedToCamera = true;
        this.restartText.anchor.setTo(1,0);
        this.btnRestart = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        var text = this.map.properties["text"];
        if (text) {
            this.levelText = game.add.bitmapText(this.map.properties["text_x"], this.map.properties["text_y"], R.font.mario, text, 30);
            this.levelText.maxWidth = 600;
            this.levelText.fill = "#fff";
        }

        this.done = false;
        this.lastCount = 0;
        this.exclusionZone = new Phaser.Ellipse(640, 360, 1280, 720);
        this.scoreExclusion = new Phaser.Rectangle(0, 0, 340, 110);
    },
    update: function() {
        this.player.handleInput();
        if (this.player2)
            this.player2.handleInput();

        lastMapInfo.score = this.score;
        lastMapInfo.boxesLost = this.boxesLost;

        this.scoreDisplay.setText("Score: " + this.score + "/" + this.requiredScore);
        this.boxDisplay.setText("Boxes lost: " + this.boxesLost + "/" + this.maxBoxesLost);

        if (this.boxesLost > this.maxBoxesLost) {
            Die(this.player);
            if (this.player2)
                Die(this.player2);
            game.time.events.add(2000, function() {
                game.state.start(R.state.gameover);
            });
        }

        if (this.score >= this.requiredScore && !this.done) {
            this.done = true;

            lastMapInfo.next_map = this.next_map;
            if (this.maxBoxesLost - this.boxesLost > 0)
                AddMedals(this.player.x, this.player.y, (this.maxBoxesLost - this.boxesLost) * 2);

            game.time.events.add(2000, function() {
                game.state.start(R.state.win);
            });
        }

        if (Medals.length > this.lastCount) {
            this.exclusionZone.setTo(640, 360, 1280 - (Medals.length * 2), 720 - (Medals.length * 2));
            this.exclusionZone.setTo(640 - (this.exclusionZone.width / 2), 360 - (this.exclusionZone.height / 2), this.exclusionZone.width, this.exclusionZone.height);
            var scr = new Phaser.Rectangle(0, 0, 1280, 720);

            for (var i = 0; i < (Medals.length - this.lastCount); i++) {
                var p = new Phaser.Point();
                do {
                    scr.random(p);
                } while (this.exclusionZone.contains(p.x, p.y) || this.scoreExclusion.contains(p.x, p.y));
                var spr = game.add.sprite(p.x, p.y, R.image.medals, Medals[this.lastCount + i]);
                spr.anchor.setTo(.5, .5);
                spr.fixedToCamera = true;
                spr.rotation = (game.rnd.frac() * (Math.PI / 2)) - (Math.PI / 4);

                var frac = game.rnd.frac();
                spr.scale.x = 1 + frac;
                spr.scale.y = 1 + frac;
            }

            this.lastCount = Medals.length;
        }

        if (this.btnRestart.isDown) game.state.start(R.state.maingame);
    }
};