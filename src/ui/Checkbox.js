var Checkbox = function(x, y, text, callback, ticked) {
    this.x = x;
    this.y = y;
    this.text = text;
    this.callback = callback;
    this.ticked = ticked || false;

    this.box_true = game.add.image(x, y, R.image.checkbox.ticked);
    this.box_true.anchor.setTo(0, .5);
    this.box_false = game.add.image(x,y, R.image.checkbox.unticked);
    this.box_false.anchor.setTo(0, .5);

    this.text = game.add.bitmapText(x, y, R.font.mario, text, 25);
    this.text.anchor.setTo(0, .5);

    var w1 = this.box_true.width + 10;
    var width = (this.text.width + w1) / 2;

    this.box_true.x -= width;
    this.box_false.x -= width;
    this.text.x -= width - w1;

    var height = Math.max(this.box_true.height, this.text.height);
    this.bounds = new Phaser.Rectangle(x - width, y - height / 2, width * 2, height);
    this.lastDown = false;

    this.updateDisplay();
};
Checkbox.prototype = {
    updateDisplay: function() {
        if (this.ticked) {
            this.box_true.alpha = 1;
            this.box_false.alpha = 0;
        } else {
            this.box_true.alpha = 0;
            this.box_false.alpha = 1;
        }
    },
    update: function() {
        var intersect = this.bounds.contains(game.input.x, game.input.y);
        if (intersect && game.input.activePointer.leftButton.justPressed(50) && !this.lastDown) {
            this.lastDown = true;
            this.ticked = !this.ticked;
            this.updateDisplay();
            this.callback(this.ticked);
        } else if (this.lastDown && !game.input.activePointer.leftButton.isDown)
            this.lastDown = false;
    }
};