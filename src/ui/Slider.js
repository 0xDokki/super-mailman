var Slider = function(x, y, width, labels, values, callback) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.minValue = values["min"];
    this.maxValue = values["max"];
    this.callback = callback;

    this.left = this.x - (this.width / 2);
    this.right = this.x + (this.width / 2);

    this.bar = game.add.nineSlice(x, y, R.image.slider.bar, null, width, 2);
    this.bar.anchor.setTo(.5, .5);

    this.end1 = game.add.sprite(this.left, y, R.image.slider.end);
    this.end1.anchor.setTo(.5, .5);
    this.end2 = game.add.sprite(this.right, y, R.image.slider.end);
    this.end2.anchor.setTo(.5, .5);

    this.txt1 = game.add.bitmapText(this.left, y, R.font.mario, labels["start"], 20);
    this.txt1.anchor.setTo(.5, -.2);
    this.txt2 = game.add.bitmapText(this.right, y, R.font.mario, labels["end"], 20);
    this.txt2.anchor.setTo(.5, -.2);

    this.pin = game.add.sprite(x, y, R.image.slider.pin);
    this.pin.anchor.setTo(.5, .6);

    this.value = values["start"] || 0;

    this.bounds = new Phaser.Rectangle(this.left, y - 20, width, 30);
    this.clicked = false;
};
Slider.prototype = {
    set value(val) {
        var a = val.clamp(this.minValue, this.maxValue) - this.minValue;
        var d = this.maxValue - this.minValue;
        this.nValue = a / d;
    },
    get value() {
        var a = this.maxValue - this.minValue;
        return this.minValue + (this.ival * a);
    },
    set nValue(val) {
        this.ival = val.clamp(0, 1);
        var d = this.right - this.left;
        this.pin.x = this.left + (this.ival * d);

        this.callback(this.value);
    },
    get nValue() {
        return this.ival;
    },
    update: function() {
        var intersect = this.bounds.contains(game.input.x, game.input.y);
        if (intersect && game.input.activePointer.leftButton.justPressed(50) && !this.clicked) this.clicked = true;
        else if (this.clicked && !game.input.activePointer.leftButton.isDown) this.clicked = false;

        if (this.clicked) {
            this.pin.x = game.input.x.clamp(this.left, this.right);

            var x1 = this.pin.x - this.left;
            var a = x1 / this.width;
            // noinspection EqualityComparisonWithCoercionJS
            if (a != this.nValue) {
                this.nValue = a;
            }
        }
    }
};