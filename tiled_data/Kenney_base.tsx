<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Kenney_base" tilewidth="70" tileheight="70" spacing="2" tilecount="169" columns="13">
 <grid orientation="orthogonal" width="35" height="35"/>
 <image source="sprs_base.png" width="934" height="934"/>
 <tile id="8">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0">
    <polygon points="0,0 0,70 70,70"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="10">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0">
    <polygon points="0,0 0,70 70,70"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="12">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="21">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="34">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="70">
    <polygon points="0,0 70,0 70,-70"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="36">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="70">
    <polygon points="0,0 70,0 70,-70"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="37">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="38">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="59">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="72">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="102">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0">
    <polygon points="0,0 0,70 70,70"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="111">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="112">
  <objectgroup draworder="index">
   <object id="3" type="coll" x="0" y="0">
    <polygon points="0,0 70,0 68.6667,32 51,57 23,66.6667 0,70"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="113">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="70">
    <polygon points="0,0 70,0 70,-70"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="138">
  <objectgroup draworder="index">
   <object id="2" type="coll" x="70" y="0">
    <polygon points="0,0 -70,0 -70.6667,25 -55.6667,53.6667 -24,66.6667 0,70"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="163">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="164">
  <objectgroup draworder="index">
   <object id="1" type="coll" x="0" y="0" width="70" height="70"/>
   <object id="2" type="coll" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
</tileset>
